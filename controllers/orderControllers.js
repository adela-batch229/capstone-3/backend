const Order = require("../models/Orders.js");
const User = require("../models/Users.js");
const Item = require("../models/Products.js")


const bcrypt = require("bcrypt")
const auth = require("../auth.js")

// order checkout
module.exports.createOrder = async (data) => {
	if(data.isAdmin == false){
		if(data.user == null) {
			return false;
		} else {
			const product = await Item.findById(data.order.product.productId);
			if(product.isActive){
				let newOrder = new Order({
					userId: data.user,
					products: {
						productId : data.order.product.productId,
						quantity : data.order.product.quantity
					},
					totalAmount: product.price * data.order.product.quantity
				})
				await newOrder.save();
				return true;
			} else {
				return false
			}
		}
	} else {
		return false
	}
}

/*retrieve user orders*/
module.exports.getOrders = async (data) =>{
	return await Order.find({userId: data.user}).then(result => {
		return result;
	})
}

/*get all orders*/
module.exports.getAllOrders = async (data) =>{
	if(data.isAdmin){
		return await Order.find().then(result => {
			return result
		})
	} else {
		return false	
	}
}
