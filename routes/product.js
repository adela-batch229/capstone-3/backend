const express = require("express");
const router = express.Router();

const auth = require ("../auth.js")
const productController = require("../controllers/productControllers.js")


// Create Item
router.post("/create", auth.verify, (req,res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Retrieve all active items
router.get("/active-items", (req,res) => {

	productController.getAllActiveItems().then(resultFromController => res.send(resultFromController));
});


// Retrieve single product
router.get("/search/:itemId", (req,res) => {

	productController.getItem(req.params).then(resultFromController => res.send(resultFromController));
});

// update Item
router.put("/update/:itemId", auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateItem(req.params, req.body, data).then(resultFromController => res.send(resultFromController));
});

// archive item
router.put("/archive/:itemId", auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveItem(req.params,req.body,data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;

// get all items
router.get("/allItems", auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.getAllItems(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;

//
router.put("/activate/:itemId", auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.activateItem(req.params,req.body,data).then(resultFromController => res.send(resultFromController))
})